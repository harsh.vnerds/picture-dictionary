<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::where('active', '=', 1)->where('is_delete', '=', 0)->get();
        return view('welcome', compact('categories'));
    }

    public function save(Request $request)
    {
        $id = $request->id;
        // dd($id);
        if (empty($id) && $id === '0') { //new
            $request->validate([
                'category_name' => 'required|unique:categories,category_name',
                'placeholder_img_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'main_img_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $msg = [];

            $category = $request->category_name;
            $exist = Category::where('category_name', '=', $category)->get();
            if (count($exist) != 0) {
                return back()
                    ->withErrors('errors', $msg);
            }

            $cat = new Category();
            $cat->category_name = $request->category_name;

            if (!empty($request->placeholder_img_path)) {
                $cat->placeholder_img_path = time() . '.' . $request->placeholder_img_path->extension();
                $request->placeholder_img_path->move(public_path('images'), $cat->placeholder_img_path);
            }
            if (!empty($request->main_img_path)) {
                $cat->main_img_path = time() . 'main.' . $request->main_img_path->extension();
                $request->main_img_path->move(public_path('images'), $cat->main_img_path);
            }
            $cat->active = 1;
            $save = $cat->save();
            if ($save) {
                $msg = 'Data added successfully.';
            } else {
                $msg = "Not saved!";
            }
            return redirect('/')->with('msg', $msg);
        } else { //edit
            // dd($id);
            $request->validate([
                'category_name' => "required|unique:categories,category_name," . $id,
                'placeholder_img_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'main_img_path' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $category = Category::find($id);
            $category->category_name = $request->category_name;

            /* $exist = Category::where('category_name', '=', $request->category_name)->get();
            if (count($exist) > 1) {
                return back()
                    ->withErrors('errors', 'Duplicate name.');
            } */

            if (!empty($request->placeholder_img_path)) {
                $category->placeholder_img_path = time() . '.' . $request->placeholder_img_path->extension();
                $request->placeholder_img_path->move(public_path('images'), $category->placeholder_img_path);
            }
            if (!empty($request->main_img_path)) {
                $category->main_img_path = time() . 'main.' . $request->main_img_path->extension();
                $request->main_img_path->move(public_path('images'), $category->main_img_path);
            }
            $save = $category->save();
            if ($save) {
                $msg = 'Data updated successfully.';
            } else {
                $msg = "Not updated!";
            }
            return redirect('/')->with('msg', $msg);
        }
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('add', compact('category'));
    }

    public function editSave($id)
    {
    }

    public function delete($id)
    {
        $category = Category::find($id);
        $category->is_delete = 1;
        $category->delete_at = Date('Y-m-d H:i:s');
        $save = $category->save();

        return redirect('/');
    }
}
