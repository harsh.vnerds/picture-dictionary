<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class ApiController extends Controller
{

    public function getData(Request $request)
    {
        try {
            if (!empty($request->id)) {
                $result = Category::where('id', '=', $request->id)->where('active', '=', 1)->where('is_delete', '=', 0)->orderby('category_name')->paginate(10);
                $result[0]->placeholder_img_path = env('BASE_URL', false) . "images/" . $result[0]->placeholder_img_path;
                $result[0]->main_img_path = env('BASE_URL', false) . "/images/" . $result[0]->main_img_path;
            } else {
                $result = Category::where('active', '=', 1)->where('is_delete', '=', 0)->orderby('category_name')->paginate(10);
                foreach ($result as $res) {
                    $res->placeholder_img_path = env('BASE_URL', false) . "images/" . $res->placeholder_img_path;
                    $res->main_img_path = env('BASE_URL', false) . "images/" . $res->main_img_path;
                }
            }
            $resultData = $result->toArray();
            $categoryData['first_page_url']=$resultData['first_page_url'];
            $categoryData['current_page']=$resultData['current_page'];

            $categoryData['last_page_url']=$resultData['last_page_url'];
            $categoryData['from']=$resultData['from'];
            $categoryData['first_page_url']=$resultData['first_page_url'];
            $categoryData['next_page_url']=$resultData['next_page_url'];
            $categoryData['path']=$resultData['path'];
            $categoryData['per_page']=$resultData['per_page'];
            $categoryData['prev_page_url']=$resultData['prev_page_url'];
            $categoryData['to']=$resultData['to'];
            $categoryData['total']=$resultData['total'];
            $categoryData['data']=$resultData['data'];


           
          
            $data['success'] = true;
            $data['message'] = "Data get successfully.!";
            $data['data'] = $categoryData;
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['success' => FALSE, 'message' => $e->getMessage()], 200, [], 5);
        }
    }

    /*  public function insertData(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'category_name' => 'required',
                    'placeholder_img_path' => 'required|mimes:png,jpg,jpeg,bmp',
                    'main_img_path' => 'required|mimes:png,jpg,jpeg,bmp',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $result = new Category();
            $result->category_name = $request->category_name;
            $img_placeholder = $request->file('placeholder_img_path');
            $result->placeholder_img_path = basename($img_placeholder->store('public/images'));
            $img_main = $request->file('main_img_path');
            $result->main_img_path = basename($img_main->store('public/images'));
            // $result->placeholder_img_path = $request->file('placeholder_img_path')->getClientOriginalName();
            //$request->file('main_img_path')->store('public/images');
            //$result->main_img_path = $request->file('main_img_path')->getClientOriginalName();

            $result->created_by = $request->login_user_id;
            $result->created_at = Date("Y-m-d H:i:s");
            $result->active = 1;
            $save = $result->save();
            if ($save) {
                $data['message'] = "Data added successfully.!";
            } else {
                $data['message'] = "Data not added.!";
            }
            $data['success'] = true;
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['success' => FALSE, 'message' => $e->getMessage()], 200, [], 5);
        }
    }

    public function updateData(Request $request)
    {
        try {

            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }
            $category_name = $request->category_name;
            $placeholder_img_path = $request->placeholder_img_path;
            $main_img_path = $request->main_img_path;
            if ($request->id != "") {
                $result = Category::find($request->id);
                if ($category_name != "")
                    $result->category_name = $category_name;
                if ($placeholder_img_path != "") {
                    $result->placeholder_img_path = basename($request->file('placeholder_img_path')->store('public/images'));
                }
                if ($main_img_path != "") {
                    $request->file('main_img_path')->store('public/images');
                    $result->main_img_path = basename($request->file('main_img_path')->store('public/images'));
                }

                $request->updated_by = $request->login_user_id;
                $request->updated_at = Date("Y-m-d H:i:s");
                $save = $result->save();
                if ($save) {
                    $data['message'] = "Data updated successfully.!";
                } else {
                    $data['message'] = "Data not updated.!";
                }
            }
            $data['success'] = true;
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['success' => FALSE, 'message' => $e->getMessage()], 200, [], 5);
        }
    }

    public function deleteData(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'id' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            if ($request->id != "") {
                $result = Category::find($request->id);
                $result->is_delete = 1;
                $result->deleted_by = $request->login_user_id;
                $save = $result->save();
                if ($save) {
                    $data['message'] = "Data deleted successfully.!";
                } else {
                    $data['message'] = "Data not deleted successfully.!";
                }
            }
            $data['success'] = true;
            return response()->json($data);
        } catch (\Exception $e) {
            return response()->json(['success' => FALSE, 'message' => $e->getMessage()], 200, [], 5);
        }
    } */
}