<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Category extends Model
{
    use HasFactory;
    use Sortable;

    protected $fillable = [
        'category_name', 'placeholder_img_path', 'main_img_path', 'active', 'is_delete', 'delete_at'
    ];
    public $sortable = ['id', 'category_name', 'created_at', 'updated_at'];
}