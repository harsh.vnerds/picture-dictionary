@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="card card-primary">
                <div class="card-header">
                    Category
                </div>

                @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                <li class="text-danger">{{ $error }}</li>
                @endforeach
                @endif
                <form class="form-horizontal" id="validateForm" method="POST" enctype="multipart/form-data" action="{{url('/save')}}">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <input type="hidden" name="id" value="{{$category->id ?? 0}}" />
                            <div class="form-group col-md-12">
                                <label for="category_name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-12">
                                    <input type="text" name="category_name" id="category_name" class="form-control" value="{{$category->category_name ?? ''}}" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="placeholder_img_path" class="col-sm-6 col-form-label">Placeholder Image</label>
                                <div class="col-sm-6 row">
                                    @if(isset($category->placeholder_img_path))
                                    <img src="{{asset('images/'.$category->placeholder_img_path)}}" width="25" height=50" class="col-sm-6" />
                                    @endif
                                    <input type="file" name="placeholder_img_path" class="col-sm-6" />
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="main_img_path" class="col-sm-6 col-form-label">Main Image</label>
                                <div class="col-sm-6 row">
                                    @if(isset($category->main_img_path))
                                    <img src="{{asset('images/'.$category->main_img_path)}}" width="25" height=50" class="col-sm-6" />
                                    @endif
                                    <input type="file" name="main_img_path" class="col-sm-6" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{url('/')}}"><button type="button" class="btn btn-info">Back</button></a>
                        <button type="submit" class="btn btn-info float-right">Save</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection