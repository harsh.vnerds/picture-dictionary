@extends('layouts.app')
@section('content')
<div class="row">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    Categories List
                </div>
                <div class="form-group row" style="margin:1%;">
                    <a href="{{url('/add')}}" class="btn btn-primary btn-sm">New</a>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td>Sr.</td>
                            <td>Category Name</td>
                            <td>Placeholder Image</td>
                            <td>Main Image</td>
                            <td colspan="2">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($categories as $cat)
                        <tr>
                            <td>{{ $loop->index+1 }} </td>
                            <td>{{$cat->category_name}}</td>
                            <td>
                                @if(!empty($cat->placeholder_img_path))
                                <img src="images/{{$cat->placeholder_img_path}}" width="25" height="25" />
                                @endif
                            </td>
                            <td>
                                @if(!empty($cat->main_img_path))
                                <img src="images/{{$cat->main_img_path}}" width="25" height="25" />
                                @endif
                            </td>
                            <td>
                                <a href="{{url('edit',$cat->id)}}"><img width="25px" height="25px" src="{{ asset('images/edit.png') }}" alt="tag"></a>
                            </td>
                            <td>
                                <form action="{{url('delete',$cat->id)}}" method="post">
                                    @csrf
                                    <button class="btn" type="submit"><img width="25px" height="25px" src="{{ asset('images/delete.png') }}" alt="tag"></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection